== Introduction ==

This module provides voicemail capabilities to phone extensions created with either VoIP Node (http://drupal.org/project/voipnode/) or VoIP User (http://drupal.org/project/voipuser/) modules.

NOTE: This version of voicemail module is designed for Drupal 6.x. It should work on both Unix platforms and Windows, but the latter has not been tested.


== Installation ==

1. Install the configure the voicemail_node (aka. VoIP Node Voicemail) module and its dependencies.

2. Configure audiofield to have player(s) for mp3 and/or wav.

3. Enable voipnode on a content type.

4. Enable voicemail on the voipnode for that content type.

5. Configure the settings of the newly created fields.


== Usage ==

The Voicemail module automatically creates a new "Voicemail box" tab for each of the content types that have have voicemail enabled.

From a developer's perspective, the module provides a collection of VoIP Drupal scripts that cover the basic recording, playback and management of messages associated with the different voicemail boxes in the system. You need to explicitly call those scripts from inside your extension to offer voicemail features to your callers.


== Global configuration ==

To change the default status of new voicemail boxes created in the system:

1. Go to admin/settings/voipnode/voicemail

2. Set the default voicemail box status to either Enable or Disable


== About ==

This module has been originally designed and implemented by the MIT Center for Civic Media (http://civic.mit.edu/) as part of the VoIP Drupal (http://drupal.org/project/voipdrupal/) initiative.


== Development notes ==

Two modules?

Just to separate the core voicemail on an extension and the node related functions.  The former could just be an include in the latter to keep module numbers down; unless someone wants to make a voicemail on an extension which is not a node.
